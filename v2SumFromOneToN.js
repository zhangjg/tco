const tco = require('./lib.js').tco;
function v2SumFromOneTo(n){
  const _sum = tco((ret,index)=>{
    if(index == 0) return ret;
    return _sum(ret+index,index-1);
  });
  return _sum(0,n);
}
module.exports={
  v2SumFromOneTo,
};

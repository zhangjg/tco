const {v2SumFromOneTo} = require('./v2SumFromOneToN.js');
test(
  "v2SumFromOneTo100",
  ()=>{
    expect(v2SumFromOneTo(100)).toBe(5050);
  }
);
test(
  "v2SumFromOneTo1E5",
  ()=>{
    try{
      v2SumFromOneTo(1E5);
      expect(1).toBe(1);
    }
    catch(err){
      expect(err).toBe(null);
    }
  }
);

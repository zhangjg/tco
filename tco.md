
# Table of Contents

1.  [TCO 之缘起](#orga2d868a)
2.  [跳床函数的实现和使用](#org900d8d0)
3.  [`tco()` 的实现和使用](#orgdc1bda0)
4.  [`tco()` 分析](#org726a4d8)
5.  [`tco()` 的问题](#org0115619)
6.  [抓 "鬼" 记](#orge86ab14)
7.  [`tco()` 的数学原理](#org644d8ea)


<a id="orga2d868a"></a>

# TCO 之缘起

第一个问题是为什么要有尾调优化. 要知道我们平常的实践中怎么遇到过尾调优化的情形实际上并不多的.

首先是, 这是两个问题. 我们先说为什么有尾调优化的需要. 简单来说, 就是递归函数要正常的工作,
需要尾调优化, 而递归函数, 未必最高效, 但是很多情况, 却最容易实现.
如果没有尾调优化, 很多递归函数实际上是没法工作的. 这不在于算法的效率问题, 而是算法无法工作.
但是有了尾调优化, 使用递归来实现一个算法, 就只效率的问题了.

一句话, 递归在编程中的作用是非常重要的, 所以很多语言, 本身是支持尾调优化的. 比如 C 语言的
GCC 实现, lua, 大部分的 Lisp 方言, 还有就是 ES6.

ES6 是现在的 Javascript 语言的 2015 年定下的规范. 大部分的目标, V8 JS 引擎都实现了,
但是对于尾调优化 (TCO) 的承诺, 还没有实现, 实际上, V8 [6.14.4~7.10.1](https://node.green/#ES2015-optimisation) 实现过,
但是最后并没有在稳定版本中得以支持. 不仅仅是 V8, 真实的情况是, [大部分的JS 引擎](https://kangax.github.io/compat-table/es6/) 都没有实现.

实际有需求, 但是现实不支持, 程序员就要发挥自己的 Hacker 精神. 今天给大家分享的就是两个实现.
今天介绍的是 Javascript 语言实现的, 理解之后, 在其他的语言上一样可以很容易实现.
因为着两个实现, 是非常简短的, 甚至比想象的还要短.

在介绍这两个方法前, 我们先来实现一个简单的递归函数, 计算从 1 加到 n 的函数.
<a id="orgdfea625"></a>

    "use strict"
    function sumFromOneTo(n){
      function _sum(ret,index){
         if(index == 0) return ret;
         return _sum(ret+index,index-1);
      }
      return _sum(0,n);
    }

ES6 对尾调的支持, 也要求必须是在 **严格** 模式下才工作的. 这一点需要注意. 现在的情况,
就是在严格模式下 V8 是没有实现尾调优化.

    npm test ^sumFromOneTo100 #test pass
    npm test ^sumFromOneTo1E5 #test fail

下面是我们的测试代码:

    const {sumFromOneTo}=require('./sumFromOneToN.js');
    function testHelp(n){
      let ret=0;
      for(let i=1;i<=n;++i){
        ret +=i;
      }
      return ret;
    }
    
    test(
      "sumFromOneTo100",
      ()=>{
        let num = 100;
        expect(sumFromOneTo(num)).toBe( testHelp(num) );
      }
    )
    
    test(
      "sumFromOneTo1E5",
      ()=>{
        let num = 1E5;
        let answer = testHelp(num);
        try{
          expect( String(sumFromOneTo(num)) ).toBe( String(answer) );
        }catch(err){
          console.log(err.message)
          expect(err).toBe(null)
        }
      }
    )

我们希望上面定义的 `sumFromOneTo(n)` 的代码的结构, 在使用我们自己 Hacker
的尾调优化的后, 可以尽可能的保留, 改动越小越好.

下面使用的两个函数 `trampoline()` 和 `tco()` 是从 [阮一峰 ECMAscript 6 入门](http://es6.ruanyifeng.com/#docs/function#%E5%B0%BE%E8%B0%83%E7%94%A8%E4%BC%98%E5%8C%96) 来的.

这两个函数, 在不同的地方, 我看到过多次. 每次都觉得自己理解了, 但是等到自己用的时候,
却发现自己还是写不出来. 尤其是 `tco()`. 直到最近, 我才真正的理解了这两个函数.


<a id="org900d8d0"></a>

# 跳床函数的实现和使用

先上代码.

    function trampoline(f){
      while(typeof f == 'function'){
        f = f();
      }
      return f;
    }

这段代码非常的简单, 我想大家都能明白. 但是我想, 只看这段代码很难想到如何用它来做尾调优化.
下面是使用这个代码的示例:
<a id="orgccc0e2c"></a>

    function v1SumFromOneTo(n){
      function _sum(ret,index){
        if(index == 0) return ret;
        return _sum.bind(null,ret+index,index-1);
      }
      return trampoline(_sum.bind(null,0,n));
    }

现在我们测试一下:

    npm test v1SumFromOneTo100
    npm test v1SumFromOntTo1E5

我们的代码通过了测试， 无论是 `n=100` 还是 `n=1E5`.
我们的测试代码为:

    const {v1SumFromOneTo}=require("./v1SumFromOneToN.js");
    test(
      "v1SumFromOneTo100",
      ()=>{
        expect(v1SumFromOneTo(100)).toBe(5050)
      }
    )
    test(
      "v1SumFromOneTo1E5",
      ()=>{
        try{
          v1SumFromOneTo(1E5)
          expect(1).toBe(1);
        }catch(err){
          console.log(err.message)
          expect(err).toBe(null);
        }
      }
    )


<a id="orgdc1bda0"></a>

# `tco()` 的实现和使用

刚才看到了跳床函数的实现以及用法. 但是有人觉得不直观, 实现了更直观的新方法,
就叫 `tco(f)`. 这个方法就不是那么的好理解了, 今天我们主要的就是要理解这个方法.
首先还是先看这个方法的实现:

    function tco(f){
      var value;
      var active=false;
      let accumulated=[];
      return function acculator(){
        accumulated.push(arguments);
        if(!active){
          active = true;
          while(accumulated.length){
    	value = f.apply(this,accumulated.shift())
          }
          active = false;
          return value;
        }
      };
    }

这个代码, 看起来好像也不难理解. 但如何用它来完成尾调优化, 和 `trampoline()` 一样, 
也难以想象出来. 我们看看怎么使用:

    function v2SumFromOneTo(n){
      const _sum = tco((ret,index)=>{
        if(index == 0) return ret;
        return _sum(ret+index,index-1);
      });
      return _sum(0,n);
    }

那么, 可以工作吗? 我们测试一下:

    npm test v2SumFromOneTo100
    npm test v2SumFromOneTo1E5

以下是我们的测试代码:

    const {v2SumFromOneTo} = require('./v2SumFromOneToN.js');
    test(
      "v2SumFromOneTo100",
      ()=>{
        expect(v2SumFromOneTo(100)).toBe(5050);
      }
    );
    test(
      "v2SumFromOneTo1E5",
      ()=>{
        try{
          v2SumFromOneTo(1E5);
          expect(1).toBe(1);
        }
        catch(err){
          expect(err).toBe(null);
        }
      }
    );

现在我们看看使用 `tco`, `trampoline` 写的递归与原始的递归之间的差距.

![img](./sumFromOneToN.png)

使用 `tco` 来写递归, 似乎略胜一筹.


<a id="org726a4d8"></a>

# `tco()` 分析

使用一般的递归算法, 计算从 1 加到 1E5 的和的时候, 也就是我们的 `sumFromOneTo(n)` 的做法,
在测试的时候, 出现了栈溢出. 这说明了 V8 现在还没有实现尾调优化. 而现在的测试, 顺利的通过了,
这说明, 我们的 `tco(f)` 函数看起来真的做了尾调优化. 现在我们对 `tco(f)` 函数本身作一点点分析,
搞清楚 `tco(f)` 是如何作尾调优化的.

在 `tco(f)` 函数被调用的时候,  `tco(f)` 函数本身会执行一次, 然后返回其内部的 `acculator()`
函数, `acculator()` 函数是 `tco(f)` 内部的一个函数, 所以可以访问 `tco(f)` 函数内部定义的变量,
 `value`, `active`, `accumumulated`, 以及参数 `f`. 在 `tco(f)` 调用结束后,
 `acculator()` 函数被返回. tco 的参数 `f`, 以及内部变量不能被垃圾回收,
因为作为结果返回出去的 `acculator()` 还维持对它们的引用. 这基本上就是闭包的工作原理.

所以从 `tco(f)` 函数本身看, 实际上是很简单的, 就是创建了一个闭包而已.

现在我们进一步的分析其中的内部函数, `acculator()`. 这个函数被返回到外面, 但是还没有被调用的时候,
`accumumulated` 数组是空的, 第一次调用 `acculator()` 的时候, 不管实际传来的参数是什么,
我们都把它们作为一个整体加到 `accumumulated` 数组的最后, 也就是说
`accumulated.push(arguments)` 这句执行之后, 数组 `accumumulated` 的长度是 1.
第一次执行, `active` 变量当然是 `false`, 所以可以进入到接下来的 `if` 语句中.
然后我们遇到了 `while` 语句, 按照我们的分析, `accumumulated` 的长度是 1.
所以, `while` 语句可以进入. 在 `while` 循环中, `f.apply(this,accumulated.shift())`
实际上是两个动作.

1.  `accumumulated.shift()`
    
    `array.shift()` 是把数组的第一个元素作为结果返回, 同时从数组中删除它. 所以这一步之后,
    `accumumulated` 的长度从 1 变成了 0.
2.  `f.apply(this,arguments)`
    这是 Javascript 中函数是对象的体现之一, 我们知道 `f` 是一个函数,
    但是现在这个函数居然也有一个叫做 `apply` 的方法, 可见函数也是对象.
    
    函数的 `apply` 方法是从 `Function` 中继承来的, 用来调用一个函数, 但是传给被调用的函数,
    但是传给这个函数的参数, 是一个运行时才能得到的类数组对象.
    
    简单来说第二步实际上就是对函数 `f` 的调用, 调用的参数就是从数组中获取的第一元素.
    **`f` 是 `tco` 的参数, 这个函数不能修改 `accumumulated`**.
    
    调用的结果保存在 value 中.

当第二步调用结束之后, `accumumulated` 的参数还是 0, 这样 `while` 循环就应该结束了.

但是, 我们得到了 1 加到 1E5 的结果, 这个结果, 显然不是调用一次 `f` 函数就能得到的.
也就是说, 我们上面的分析是有问题的. 问题在哪里?

为了更好的理解 `tco` 函数, 可以通过断点调试来看看执行的过程. 在我这样作的过程中, 我发现,
断点调试让我更加的困惑了. 更加的不清楚整个程序的执行过程了.

所以我使用了第二种方法, 直接在 `tco` 函数中加入一些输出语句:

    function tco(f){
        var value;
        var active=false;
        var accumulated=[];
        console.log("a");
        return function acculator(){
    	console.log("b");
    	accumulated.push(arguments);
    	if(!active){
    	    active = true;
    	    while(accumulated.length) {
    		console.log("c");
    		value = f.apply(this,accumulated.shift());
    		console.log("d");
    	    }
    	    active = false;
    	    return value;
    	}
        }
    }
    
    function sumFromOneTo(n){
        var _sum=(tco((ret,index)=>{
    	return index == 0? ret: _sum(ret+index,index-1);
        }));
        return _sum(0,n);
    }
    
    if(require.main == module ){
        console.log(`sumFromOneTo(1E5)=${sumFromOneTo(1E5)}`);
    }

执行:

    node tco-add-log.js

最后的答案是正确的, 但是输出的日志却出乎我的意料:

除了最开始一段为 "a" "b" "c" "d" 外, 中间的都是以在重复的 "d" "c" "b" 序列.
而我期望出现的 "b" "c" "d" 一次也没有出现.

"d" "c" "b"  序列, 告诉我们, 在 c 执行完之后, 直接跳到了 b, 然后有才回来执行 "d".
而且长长的输出序列, 也清楚的告诉我们 `while` 循环不止执行了一次. 而要想 `while` 循环继续执行,
只能是 `f` 被调用的时候, 修改了 `accumumulated` 的长度. 但是之前我们已经分析过, `f`
作为一个外部的函数, 是没法修改 `accumumulated` 这个内部函数的, 除非传递进来的
`f` 就是我们已经返回出去的 `acculator()`, 但是很容易验证 `f` 和 `acculator()` 不是同一个函数.

调试, 英文叫 Debug, 就是把代码中的 `bug` 抓出来. 现在给我的感觉不是代码中有虫了,
而是代码中有鬼了. 这个 "鬼" 1)偷偷的修改了 `accumumulated` 的长度; 2)修改了程序执行的顺序.
要么是两个合作的小 "鬼", 要么是一个厉害的大 "鬼". 但无论是那种情况, 都不简单.
抓 "虫" 很容易, 因为我们能看到虫子. 抓鬼所以不简单, 因为我们看不到 "鬼".

这也就是为什么, 这段不长的代码, 看过多次后, 我依旧不能理解它, 更加的不能实现它.


<a id="org0115619"></a>

# `tco()` 的问题

现在的问题是, `tco` 这样的代码, 没多少工作量, 而且像我这样的程序员都能找得到(虽然没能理解),
为啥 V8 团队不用这样的代码来解决 TCO 的问题呢? 我认为 V8 的语言专家一定是知道 `tco(f)` 的实现的,
而且一定能理解的. 从这个思路重新考虑, 最后帮助我找到了, `tco()` 中的 "鬼", 也最终帮助我理解了 `tco()` 函数.

`tco()` 能按照上面示例的那样工作, 但是也 **只能** 按照上面的示例那样 **才** 有效.
如果把 [`sumFromOneTo(n)`](#orgdfea625) 内部定义的 `_sum` 传给 `tco()` 它是不能工作的. 例如:

    function v3SumFromOneTo(n){
      function _sum(ret,index){
        if(index == 0) return ret;
        return _sum(ret+index,index-1);
      }
      return tco(_sum)(0,n);
    }
    module.exports={
      v3SumFromOneTo,
    };

我们测试一下看看:

    npm test v3SumFromOneTo

出现栈溢出了。测试代码:

    const {v3SumFromOneTo} = require('./v3SumFromOneToN.js');
    test(
      "v3SumFromOneTo1",
      ()=>{
        try{
          expect( v3SumFromOneTo(1) ).toBe(1);
        }catch(err){
          console.log(err);
          expect(err).toBe(null);
        }
      }
    )
    test(
      "v3SumFromOneTo100",
      ()=>{
        try{
          expect(v3SumFromOneTo(100)).toBe(5050);
        }catch(err){
          expect(err).toBe(null);
        }
      }
    );
    test(
      "v3SumFromOneTo1E5",
      ()=>{
        try{
          v3SumFromOneTo1E5(1E5);
          expect(1).toBe(1);
        }catch(err){
          expect(err).toBe(null);
        }
      }
    );


<a id="orge86ab14"></a>

# 抓 "鬼" 记

现在对比 `v2SumFormOneTo(n)` 和 `v3SumFromOneTo(n)` 这两个函数,
看看到底什么地方使得 `v2SumFormOneTo(n)` 工作, 而 `v3SumFromOneTo(n)` 却出错.

![img](./p2.png)

仔细对比这两个代码, 我们发现, 明显的区别是, `v3SumFromOneTo(n)` 的内部函数,
定义好之后, 传给 `tco()` 函数的, 是这个的原因吗? 我们修改 `v2SumFormOneTo(n)`,
看看是不是这个原因造成的.

    const {tco}= require('./lib.js');
    
    function v4SumFromOneTo(n){
      var _sum = function(ret,index){
        return index == 0? ret: _sum(ret+index,index-1);
      }
      return tco(_sum)(0,n);
    }

测试一下:

    npm test v4SumFromOneTo

失败了, 看来我们的猜测不对, 愿意不在于是我们定义好一个函数后, 在传递给 `tco()`,
还是在调用 `tco()` 的时候才创建函数.

对比 [`v1SumFromOneTo(n)`](#orgccc0e2c), 我发现, 在那个版本中, 需要把 `tco(f)` 返回的结果保存的一个变量中,
那么我们这这样作吧.

    function v5SumFromOneTo(n){
      var _sum = function(ret,index) {
        return index == 0? ret: _sum(ret+index,index-1);
      }
      _sum = tco(_sum);//THIS IS KEY
      return _sum(0,n);
    }

测试一下:

    npm test v5SumFromOneTo

通过了. 但是为什么?! 似乎, 我们还是没找到 "鬼" 啊!

![img](./p3.png)

但是现在我们知道, "鬼" 不在 `tco()` 里面, 而在我们的 `xxSumFromOneTo(n)` 中. 我们已经缩小了范围, 
已经把那个鬼逼到了死角, 那行注释了 "THIS IS KEY" 的代码中了.

这关键性的一行是作了什么呢? 它把 `tco(f)` 的返回值赋给了 `_sum` 变量, 所以 **在匿名函数中调用 `_sum`,
现在就是 `tco` 的返回值, 也就是  `acculator()`**, 而不是我们认为的传递给 `tco()` 的那个参数 `f`.

为了方便期, 我们再对函数作一点改动:

    function v6SumFromOneTo(n){
      function inerSum(ret,index){
        return index == 0 ?
    	   ret :
    	   _sum(ret+index,index-1);//LOOK AT ME
      }
      let _sum = tco(inerSum);
      return _sum(0,n);
    }

它工作吗?

    npm test v6SumFromOneTo

现在让我们对 `tco()` 相关的函数的关系作个梳理.

1.  `v6SumFromOneTo(n)` 变量 `_sum` 是什么? 
    
    很清楚, 这是 `tco()` 返回的函数, 也就是 `acculator()`.
2.  `inerSum` 中的调用的 `_sum` 是什么?
    
    就是 `v6SumFromOneTo(n)` 中的变量 `_sum`. 也就是说, 1 和 2 是同一个东西.
3.  `toc(f)` 中的 `f` 是什么?
    
    是这里定义的 `innerSum`.

现在我们来分析以下 `tco()` 正常工作时的调用栈.

1.  v6SumFromOneTo(n) 被调用, **等待** 内部的 `_sum(0,n)` 执行结果.
2.  `_sum(0,n)` 也就是 `acculator(0,n)`  进入 `while` 循环, **等待** `f(0,n)` 也就是 `inerSum(0,n)` 的结果
3.  `inserSum(0,n)` **等待** `_sum(n+0,n-1)` 也就是 `acculator(n+0,n-1)` 的结果.
4.  `accumulated(n+0,n-1)` 向数组 `accumulated` 中添加 `{0:n+0,1:n-1}` 对象, 不进入 `if`, 返回 `undefine`, 返回 2 继续 `while`.

现在我们就清楚的看到循环是怎么形成的了.


<a id="org644d8ea"></a>

# `tco()` 的数学原理

`trampoline()` 非常的明晰, 它把递归转化成循环的方式简单粗暴. 
在递归函数, 在需要调用自己一般获得一个值的地方, 修改为了返回一个函数,
以延迟到 `trampoline()` 函数中执行. 这样在 `trampoline()` 中, 只要返回值是一个函数, 
就说明递归还没有结束, 否则递归就结束了, 所以 `trampoline()` 非常的简单.

但是 `tco()` 函数却不是那么简单, 为什么呢? 上面我们说明了 `tco()` 的工作原理, 
这里又继续的问为什么, 再把上面的过程重复一遍, 显然不是我想要的答案.

我想问的是, 为什么 `tco()` 不像 `trampoline()` 那样一眼就让我们看穿呢? 
我知道, `tco()` 本身比 `trampoline()` 复杂, 但是抛开用这两个函数来作尾调优化,
单单看这两个函数, 他们的复杂度之间的差异, 我感觉和用来作尾调优化后的难度非常的不一样.
无论是不是用来作尾调优化, `trampoline()` 的复杂度不变. 但是用来作尾调优化后, 
`tco()` 的复杂度提高了. 这是我的感受, 为什么会这样? 其中有什么道理? 这是我真正想问的.

从上一节的分析来看, 显然 `tco()` 函数本身是没办法独自完成 **把递归转化为循环** 的任务的.
和 `trampoline()` 对比一下就非常明白. `trampoline()` 函数只对传入的函数 `f` 
有要求,而 `tco()` 要想正常的工作, 需要把传入的参数 `f` 的定义, 与 `tco()` 
的返回结果, 这两个结合起来才能完成工作. 

我们在无意间完成了 `tco()` 的要求, 这样才真正的完成了 **递归转化为循环** 的工作. 
因为是无意中配合了 `tco()` 函数, 才会把注意力集中到 `tco()` 内部是如何运作. 
这就想魔术师, `tco()` 成功的把我们的注意力吸引到了错误的地方.

再进一步的分析 `tco()` 函数, 真正让我们迷惑的是传给 `tco()` 函数的 `f`
函数的执行顺序. `f` 始终是在 `acculator()` 中执行的, 即使当我们认为 `f`
已经逃出 `acculator()` 函数的的时候, 它还在其中. 这让我想起来一个有趣的拓扑结构:
[莫比乌斯带](https://en.wikipedia.org/wiki/M%C3%B6bius_strip).

![img](https://upload.wikimedia.org/wikipedia/commons/b/b7/Fiddler_crab_mobius_strip.gif)

`tco()` 就是构造了一个莫比乌斯带, 而 `f` (以及迷惘时的我), 就像在这个带上的螃蟹,
企图沿着这个带子走到带子的边界, 然后翻到带子的另一面, 却没有觉察到, 这个带子只有一面.

回头看 `trampoline()` 它给我们的是一个 **环**, 一个一维的拓扑结构,
相比与 二维的 **托比乌斯带** 我们对它再熟悉不过了. 我想这就是为什么我们不能像看穿 
`trampoline()` 那样, 轻易看穿 `tco()` 的原因.


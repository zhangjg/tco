const tco = require('./lib.js').tco;
function v3SumFromOneTo(n){
  function _sum(ret,index){
    if(index == 0) return ret;
    return _sum(ret+index,index-1);
  }
  return tco(_sum)(0,n);
}
module.exports={
  v3SumFromOneTo,
};

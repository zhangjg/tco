"use strict"
function sumFromOneTo(n){
  function _sum(ret,index){
     if(index == 0) return ret;
     return _sum(ret+index,index-1);
  }
  return _sum(0,n);
}

module.exports={
  sumFromOneTo,
}

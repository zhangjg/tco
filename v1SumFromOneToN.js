const trampoline = require('./lib.js').trampoline;
function v1SumFromOneTo(n){
  function _sum(ret,index){
    if(index == 0) return ret;
    return _sum.bind(null,ret+index,index-1);
  }
  return trampoline(_sum.bind(null,0,n));
}

module.exports={
  v1SumFromOneTo,
};

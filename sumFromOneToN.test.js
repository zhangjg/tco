const {sumFromOneTo}=require('./sumFromOneToN.js');
function testHelp(n){
  let ret=0;
  for(let i=1;i<=n;++i){
    ret +=i;
  }
  return ret;
}

test(
  "sumFromOneTo100",
  ()=>{
    let num = 100;
    expect(sumFromOneTo(num)).toBe( testHelp(num) );
  }
)

test(
  "sumFromOneTo1E5",
  ()=>{
    let num = 1E5;
    let answer = testHelp(num);
    try{
      expect( String(sumFromOneTo(num)) ).toBe( String(answer) );
    }catch(err){
      console.log(err.message)
      expect(err).toBe(null)
    }
  }
)

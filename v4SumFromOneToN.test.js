const {tco}= require('./lib.js');

function v4SumFromOneTo(n){
  var _sum = function(ret,index){
    return index == 0? ret: _sum(ret+index,index-1);
  }
  return tco(_sum)(0,n);
}

function v5SumFromOneTo(n){
  var _sum = function(ret,index) {
    return index == 0? ret: _sum(ret+index,index-1);
  }
  _sum = tco(_sum);//THIS IS KEY
  return _sum(0,n);
}

test(
  "v4sumFromOneTo(1E5)",
  ()=>{
    try{
      v4SumFromOneTo(1e5);
      expect(1).toBe(1);
    }catch(err)
    {
      expect(err).toBe(null);
    }
  }
)

test(
  "v5sumFromOneTo(1E5)",
  ()=>{
    try{
      v5SumFromOneTo(1e5);
      expect(1).toBe(1);
    }catch(err)
    {
      expect(err).toBe(null);
    }
  }
)

function v6SumFromOneTo(n){
  function inerSum(ret,index){
    return index == 0 ?
           ret :
           _sum(ret+index,index-1);//LOOK AT ME
  }
  let _sum = tco(inerSum);
  return _sum(0,n);
}

test(
  "v6SumFromOneTo(1E5)",
  ()=>{
    try{
      v6SumFromOneTo(1E5);
      expect(1).toBe(1);
    }catch(err){
      expect(err).toBe(null);
    }
  }
)

function trampoline(f){
  while(typeof f == 'function'){
    f = f();
  }
  return f;
}
function tco(f){
  var value;
  var active=false;
  let accumulated=[];
  return function acculator(){
    accumulated.push(arguments);
    if(!active){
      active = true;
      while(accumulated.length){
        value = f.apply(this,accumulated.shift())
      }
      active = false;
      return value;
    }
  };
}
module.exports={
  trampoline,
  tco,
};

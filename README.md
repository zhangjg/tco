这是 TCO 技术分析的代码仓库.

TCO 文章假设 NODEJS 环境已经安装完成, 在安装文章运行代码前,
首先克隆本项目到本地.

    git clone git@bitbucket.org:zhangjg/tco.git

然后安装 jest 测试套件.

    npm install # install jest

然后就可以按照文档中的描述来执行代码了.